import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




@WebServlet("/")
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = -4751096228274971485L;

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userId = request.getParameter("un");
        String password = request.getParameter("pw");
        if (userId.contains("admin")) {
            ClientDAO clientDAO = new ClientDAO();
            List<Administrator> admins = clientDAO.getAdmins(userId, password);
            if (admins.size() != 1){
                response.setContentType("text/html");
                PrintWriter out = response.getWriter();
                out.println("<h2>ERROR</h2>");
            } else {
                HttpSession sessionhttp = request.getSession(true);
                sessionhttp.setAttribute("currentAdmin", admins.get(0));
                response.sendRedirect("admin.jsp");
            }
        }else {
            ClientDAO clientDAO = new ClientDAO();
            List<Client> clients = clientDAO.getUsers(userId, password);
            if (clients.size() != 1) {
                response.setContentType("text/html");
                PrintWriter out = response.getWriter();
                out.println("<h2>ERROR</h2>");

            } else {

                HttpSession sessionhttp = request.getSession(true);
                sessionhttp.setAttribute("currentSessionUser", clients.get(0));
                response.sendRedirect("FlightServlet");
            }
        }
    }
}
