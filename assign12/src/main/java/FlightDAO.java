import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class FlightDAO {

    public List<Flight> getAllFlights(){
        Session session = HibernateUtil.getSession();
        List<Flight> flights = (List<Flight>) session.createQuery("from Flight").list();
        session.close();
        return flights;
    }

    public void addFlight(Flight f){
        Session session = HibernateUtil.getSession();
        Transaction tx = session.beginTransaction();
        session.save(f);
        tx.commit();
        session.close();
    }

    public void updateFlight(Flight f){
        Session session = HibernateUtil.getSession();
        Transaction tx = session.beginTransaction();
        session.saveOrUpdate(f);
        tx.commit();
        session.close();
    }

    public void deleteFlight(int id){
        Session session = HibernateUtil.getSession();
        Transaction tx = session.beginTransaction();
        Flight f = (Flight)session.load(Flight.class,id);
        session.delete(f);
        tx.commit();
        session.flush() ;
    }
}
