import org.hibernate.Session;
import org.hibernate.Transaction;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/AdminServlet")
public class AdminServlet extends HttpServlet {


    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession sessionhttp = request.getSession(false);

        if (sessionhttp == null) {
            response.sendRedirect("/index.jsp");
            return;
        }else{
            if (sessionhttp.getAttribute("currentAdmin") == null){
                response.sendRedirect("/index.jsp");
                return;
            }
        }

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<table style=\"width:100%\">");
        FlightDAO flightDAO = new FlightDAO();
        List<Flight> flights = flightDAO.getAllFlights();
        for (Flight f : flights) {
            out.println(
                    "<tr><th>" + f.getIdFlight() + "</th><th>"+f.getAirPlaneType()+"</th><th>"+f.getDepartureCity()+"</th ><th>"+f.getDepartureDateTime()+"</th><th>"+f.getArrivalCity() +"</th><th>"+f.getArrivalDateTime()+"</th></tr>"
            );
        }
        out.println("</table>");

    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession sessionhttp = request.getSession(false);

        if (sessionhttp == null) {
            response.sendRedirect("/index.jsp");
            return;
        }else{
            if (sessionhttp.getAttribute("currentAdmin") == null){
                response.sendRedirect("/index.jsp");
                return;
            }
        }
        String methodType = request.getParameter("methodType");
        FlightDAO flightDAO = new FlightDAO();
        if (methodType.equals("update") || methodType.equals("add")) {
            String id = request.getParameter("idflight");
            String planetype = request.getParameter("planetype");
            String depCity = request.getParameter("depCity");
            String depTime = request.getParameter("depTime");
            String arrcity = request.getParameter("arrcity");
            String arrtime = request.getParameter("arrtime");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date depDateTime = null;
            try {
                depDateTime = sdf.parse(depTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Date arrDateTime = null;
            try {
                arrDateTime = sdf.parse(arrtime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Flight f = new Flight(Integer.parseInt(id), planetype, depCity, depDateTime, arrcity, arrDateTime);

            if (methodType.equals("add")) {
                flightDAO.addFlight(f);
                response.sendRedirect("admin.jsp");
            }else if (methodType.equals("update")) {
                flightDAO.updateFlight(f);
                response.sendRedirect("admin.jsp");
            }


        } else {
            String id = request.getParameter("idflight");
            flightDAO.deleteFlight(Integer.parseInt(id));
            response.sendRedirect("admin.jsp");
        }


    }

}