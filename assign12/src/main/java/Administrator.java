import javax.persistence.*;

@Entity
@Table(name = "administrator")
public class Administrator {

    @Id
    @Column(name = "idadministrator")
    private int idAdministrator;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    public int getIdAdministrator() {
        return idAdministrator;
    }

    public void setIdAdministrator(int idAdministrator) {
        this.idAdministrator = idAdministrator;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Administrator{" +
                "idAdministrator=" + idAdministrator +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
