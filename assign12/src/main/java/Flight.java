
import javax.persistence.*;
//import java.sql.Date;
import java.util.Date;
@Entity
@Table(name = "flight")
public class Flight {
    @Id
    @Column(name = "idFlight")
    private int idFlight;

    @Column(name = "airPlaneType")
    private String airPlaneType;

    @Column(name = "departureCity")
    private String departureCity;

    @Column(name = "departureDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date departureDateTime;

    @Column(name = "arrivalCity")
    private String arrivalCity;

    @Column(name = "arrivalDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date arrivalDateTime;

    public Flight(int idFlight, String airPlaneType, String departureCity, Date departureDateTime, String arrivalCity, Date arrivalDateTime) {
        this.idFlight = idFlight;
        this.airPlaneType = airPlaneType;
        this.departureCity = departureCity;
        this.departureDateTime = departureDateTime;
        this.arrivalCity = arrivalCity;
        this.arrivalDateTime = arrivalDateTime;
    }

    public Flight(){

    }

    public int getIdFlight() {
        return idFlight;
    }

    public void setIdFlight(int idFlight) {
        this.idFlight = idFlight;
    }

    public String getAirPlaneType() {
        return airPlaneType;
    }

    public void setAirPlaneType(String airPlaneType) {
        this.airPlaneType = airPlaneType;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public Date getDepartureDateTime() {
        return departureDateTime;
    }

    public void setDepartureDateTime(Date departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public Date getArrivalDateTime() {
        return arrivalDateTime;
    }

    public void setArrivalDateTime(Date arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "idFlight=" + idFlight +
                ", airPlaneType='" + airPlaneType + '\'' +
                ", departureCity='" + departureCity + '\'' +
                ", departureDateTime=" + departureDateTime +
                ", arrivalCity='" + arrivalCity + '\'' +
                ", arrivalDateTime=" + arrivalDateTime +
                '}';
    }
}
