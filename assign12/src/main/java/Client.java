import javax.persistence.*;

@Entity
@Table(name = "client")
public class Client {

    @Id
    @Column(name = "idclient")
    private int idClient;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;


    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Client{" +
                "idClient=" + idClient +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
