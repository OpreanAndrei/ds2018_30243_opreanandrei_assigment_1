import org.hibernate.Session;
import org.hibernate.Transaction;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/FlightServlet")
public class FlightServlet extends HttpServlet {
    //private static final long serialVersionUID = -4751096228274971485L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession sessionhttp = request.getSession(false);

        if (sessionhttp == null) {
            response.sendRedirect("/index.jsp");
            return;
        }else{
            if (sessionhttp.getAttribute("currentSessionUser") == null){
                response.sendRedirect("/index.jsp");
                return;
            }
        }
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<table style=\"width:100%\">");
        FlightDAO flightDAO = new FlightDAO();
        List<Flight> flights = flightDAO.getAllFlights();
        for (Flight f : flights) {
            out.println(
                    "<tr><th>" + f.getIdFlight() + "</th><th>"+f.getAirPlaneType()+"</th><th>"+f.getDepartureCity()+"</th ><th>"+f.getDepartureDateTime()+"</th><th>"+ "<a href=LocalTimeServlet?City=" + f.getArrivalCity() +">" + f.getArrivalCity()+ "</a>" +"</th><th>"+f.getArrivalDateTime()+"</th></tr>"
                            );
        }
        out.println("</table>");

    }


}

