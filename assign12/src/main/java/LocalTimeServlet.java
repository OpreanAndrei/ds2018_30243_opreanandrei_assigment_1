import jdk.internal.org.xml.sax.InputSource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

@WebServlet("/LocalTimeServlet")
public class LocalTimeServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException  {
        String city = request.getParameter("City");
        int firstBracket = city.indexOf('(');
        String contentOfBrackets = city.substring(firstBracket + 1, city.indexOf(')', firstBracket));
        String[] parts = contentOfBrackets.split(",");
        String urlForRequest = "http://new.earthtools.org/timezone/" + parts[0] + "/" + parts[1];
        URL obj = new URL(urlForRequest);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        int responseCode = con.getResponseCode();
        System.out.println("Response Code : " + responseCode);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer responseFromServer = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            responseFromServer.append(inputLine);
        }
        in.close();
        System.out.println(" XML RESPONSE : " + responseFromServer.toString());

        String a = responseFromServer.toString();
        int start = a.indexOf("<localtime>");
        int end = a.indexOf("</localtime>");
        String toPrintOnPage = a.substring(start+11, end);
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<h1>"+city+"->"+toPrintOnPage+"</h1>");
    }
}
