import org.hibernate.Session;

import java.util.List;

public class ClientDAO {

    public List<Administrator> getAdmins(String userId, String password){
        Session session = HibernateUtil.getSession();
        List<Administrator> admins = (List<Administrator>) session.createQuery("from Administrator where username=:userId and password=:password").setParameter("userId", userId).setParameter("password", password).list();
        session.close();
        return admins;
    }

    public List<Client> getUsers(String userId, String password){
        Session session = HibernateUtil.getSession();
        List<Client> clients = (List<Client>) session.createQuery("from Client where username=:userId and password=:password").setParameter("userId", userId).setParameter("password", password).list();
        session.close();
        return clients;
    }
}
