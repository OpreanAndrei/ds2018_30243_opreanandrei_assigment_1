import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TestForDB {
    public static void main (String...args){

        Session session = HibernateUtil.getSession();
        Transaction tx = session.beginTransaction();



        Flight f1 = new Flight();
        Flight f2 = new Flight();
        Flight f3 = new Flight();
        Flight f4 = new Flight();
        Flight f5 = new Flight();

        f1.setIdFlight(1);
        f1.setAirPlaneType("Big");
        f1.setArrivalCity("Barcelona");
        f1.setDepartureCity("Cluj");
        f1.setArrivalDateTime(new Date());
        f1.setArrivalDateTime(new Date());
        Date currentDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);

        c.add(Calendar.YEAR, 0);
        c.add(Calendar.MONTH, 0);
        c.add(Calendar.DATE, 0);
        c.add(Calendar.HOUR, 1);
        c.add(Calendar.MINUTE, 5);
        c.add(Calendar.SECOND, 20);

        Date currentDatePlusOne = c.getTime();

        f1.setArrivalDateTime(currentDatePlusOne);

        f2.setIdFlight(2);
        f2.setAirPlaneType("Medium");
        f2.setArrivalCity("Berlin");
        f2.setDepartureCity("Cluj");
        f2.setDepartureDateTime(new Date());

        c.add(Calendar.YEAR, 0);
        c.add(Calendar.MONTH, 0);
        c.add(Calendar.DATE, 0);
        c.add(Calendar.HOUR, 4);
        c.add(Calendar.MINUTE, 2);
        c.add(Calendar.SECOND, 1);

        Date currentDatePlusOne1 = c.getTime();
        f2.setArrivalDateTime(currentDatePlusOne1);

        session.save(f1);
        session.save(f2);

        tx.commit();

        List<Client> clients = (List<Client>)session.createQuery("from Client").list();
        System.out.println(Arrays.toString(clients.toArray()));

        session.close();
        HibernateUtil.shutdown();
    }
}
