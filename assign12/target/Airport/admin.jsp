<%--
  Created by IntelliJ IDEA.
  User: Dell6
  Date: 11/27/2018
  Time: 6:29 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Crud operations for admin</title>
</head>
<body>
<%
    HttpSession sessionhttp = request.getSession(false);
    if (sessionhttp == null) {
        response.sendRedirect("/index.jsp");
        return;
    }else{
        if (sessionhttp.getAttribute("currentAdmin") == null){
            response.sendRedirect("/index.jsp");
            return;
        }
    }
%>
<h1>Crud operations for admin</h1>
<a href="addflight.jsp">Add Flight</a>
<a href="AdminServlet">View Flights</a>
<a href="updateflights.jsp">Update Flight</a>
<a href="deleteflight.jsp">Delete Flight</a>
</body>
</html>
